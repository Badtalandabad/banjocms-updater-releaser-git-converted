<?php

require 'entry.php';

$response = new \JsonResponse();

$task = \SRequester::get( 'task' );

/** @var \UpdateServer $server */

switch( $task ) {
    case 'check-for-update':
        $version = \SRequester::get( 'version' );
        if ( \SReleaser::isVersion( $version ) ) {
            $response->setData( $server->checkForUpdateTask( $version ) );
        } else {
            $response->setError();
            $response->setErrorMessage( 'Invalid release version!' );
        }
        $server->sendResponse( $response );
        break;

    case 'get-latest-version':
        $version = \SReleaser::getReleaseVersion();
        $response->setData( $version );
        $server->sendResponse($response);
        break;

    case 'get-release-hashes':
        $version = \SRequester::get( 'version' );
        if ( \SReleaser::isVersion( $version ) ) {
            //TODO: add try
            $hashes = $server->getReleaseHashesTask( $version );
            $response->setData( $hashes );
        } else {
            $response->setError();
            $response->setErrorMessage( 'Invalid release version!' );
        }
        $server->sendResponse($response);
        break;

    case 'get-release-pack-hash':
        $version = \SRequester::get( 'version' );
        if ( \SReleaser::isVersion( $version ) ) {
            //TODO: add try
            $hashes = $server->getReleasePackHashTask( $version );
            $response->setData( $hashes );
        } else {
            $response->setError();
            $response->setErrorMessage( 'Invalid release version!' );
        }
        $server->sendResponse($response);
        break;

    case 'run-update':
        $server->updateTask();
        break;

    default:
        $response->setError();
        $response->setErrorMessage( 'Unknown task: "' . $task . '"' );
        $server->sendResponse( $response );
        break;
}


