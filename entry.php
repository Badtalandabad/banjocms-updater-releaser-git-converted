<?php

//Application entry

require 'Application/BaseTools/baseRequire.php';

ob_start();

\Configuration::init();

$developmentMode = \Configuration::getParameter( 'developmentMode' );
$showErrors = \Configuration::getParameter( 'showPhpErrors' );
if ( $developmentMode && $showErrors ) {
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_startup_errors', 1 );
    ini_set( 'html_errors', 1 );
    ini_set( 'display_errors', 1 );
}

$server = new \UpdateServer();
