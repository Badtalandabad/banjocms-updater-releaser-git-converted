<?php

/**
 * Defines base constants
 * */

define( 'BASE_PATH', dirname( dirname(__DIR__) ) );

define( 'APP_PATH', BASE_PATH . '/Application' );
define( 'LOG_DIR',  BASE_PATH . '/log' );
define( 'TEMP_DIR', BASE_PATH . '/tmp' );
define( 'RELEASE_DIR', BASE_PATH . '/releases' );

define( 'CONF_PATH', APP_PATH . '/Configuration' );
define( 'LIBRARY_PATH', APP_PATH . '/Library' );

define( 'EXCEPTION_ABSTRACT_PATH', APP_PATH . '/Library/Exception/ExceptionAbstract.php' );

//define( 'LAYOUT_DIR', APP_PATH . '/Layout' );
//define( 'PUBLIC_WEB_PATH', 'Public' );
//define( 'MAIN_MODULE', 'Application' );
//define( 'BASE_CONTROLLER', 'Base' );
//define( 'DEFAULT_TASK', 'display' );

