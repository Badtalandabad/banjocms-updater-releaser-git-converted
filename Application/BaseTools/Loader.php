<?php


/**
 * Class Loader
 * Finds and loads declaration files for new classes.
 * All relative paths are relative to 'Application' directory
 */
class Loader
{
    /**
     * Directories for searching, common for all classes
     *
     * @var array
     */
    private static $_commonPaths = array(
        'Library' => 'Library',
    );

    /**
     * Paths specified for the current class
     *
     * @var array
     */
    private static $_classSpecifiedPaths = array();

    /**
     * List of loaded files
     *
     * @var array
     */
    private static $_loadedFiles = array();

    /**
     * Returns common paths
     *
     * @return  array
     */
    private static function _getCommonPaths()
    {
        return self::$_commonPaths;
    }

    /**
     * Adds specified class path to the store
     *
     * @param   string  $path   Full path to the file
     *
     * @return  void
     */
    private static function _addClassSpecifiedPath( $path )
    {
        self::$_classSpecifiedPaths[$path] = $path;
    }

    /**
     * Returns specified class paths
     *
     * @return array
     */
    private static function _getClassSpecifiedPaths()
    {
        return self::$_classSpecifiedPaths;
    }

    /**
     * Clears class specified paths
     *
     * @return  void
     */
    private static function _clearClassSpecifiedPaths()
    {
        self::$_classSpecifiedPaths = array();
    }

    /**
     * Saves loaded file path in store
     *
     * @param   string  $path   Full path to the file
     *
     * @return  void
     */
    private static function _registerLoadedFile( $path )
    {
        self::$_loadedFiles[$path] = $path;
    }

    /**
     * Checks if file have already loaded. Returns true on success, otherwise false
     *
     * @param   string  $path   Full path to the file
     *
     * @return  bool
     */
    private static function _checkLoadedFile( $path )
    {
        if ( array_key_exists( $path, self::$_loadedFiles ) ) {
            return true;
        }
        return false;
    }

    /**
     * Adds common path to the path store. Nothing would happen if
     * file have already stored
     *
     * @param   string $path Relative(to the 'Application' directory)
     *                       path for searching
     *
     * @return  void
     */
    public static function addCommonPath( $path )
    {
        self::$_commonPaths[$path] = $path;
    }

    /**
     * Adds a bunch of common paths to the path store.
     * Nothing would happen if some of paths have already stored
     *
     * @param   array $paths Array of the relative(to the
     *                       'Application' directory) paths for searching
     *
     * @return  void
     */
    public static function addCommonPaths( $paths )
    {
        foreach ( $paths as $path ) {
            self::addCommonPath( $path );
        }
    }

    /**
     * Returns full paths for searching
     *
     * @param   string  $className  Full (with the namespaces) name of the class
     *
     * @return  array
     */
    public static function getSearchPaths( $className )
    {
        $relativePath = str_replace( '\\', '/', $className ) .  '.php';
        $slashPos = strrpos( $relativePath, '/' );
        $cleanClassFileName = substr( $relativePath, $slashPos ) . '.php';

        $fullPaths = array();
        foreach ( self::_getCommonPaths() as $commonLoadDir ) {
            $commonLoadDir = $commonLoadDir ? $commonLoadDir . '/' : '';

            $fullPaths[] = APP_PATH . '/' . $commonLoadDir . $relativePath;
        }
        foreach ( self::_getClassSpecifiedPaths() as $classLoadDir ) {
            $classLoadDir = $classLoadDir ? $classLoadDir . '/' : '';

            $fullPaths[] = APP_PATH . '/' . $classLoadDir . $cleanClassFileName;
            if ( $relativePath != $cleanClassFileName ) {
                $fullPaths[] = APP_PATH . '/' . $classLoadDir . $relativePath;
            }
        }

        return $fullPaths;
    }


    /**
     * Returns path to the class declaration file
     *
     * @param   string  $className  Full (with the namespaces) name of the class
     *
     * @return  string|null
     */
    public static function findClassFilePath( $className )
    {
        foreach ( self::getSearchPaths( $className ) as $path ) {
            if ( file_exists( $path ) ) {
                return $path;
            }
        }

        return null;
    }

    /**
     * Loads file with class declaration. Returns true on success, otherwise false
     *
     * @param   string  $className  Full (with the namespaces) name of the class
     *
     * @throws  \LoadException   If trying to load some file twice
     *
     * @return  bool
     */
    public static function load( $className )
    {
        $paths = self::getSearchPaths( $className );
        foreach ( $paths as $path ) {
            if ( file_exists( $path ) ) {

                if ( self::_checkLoadedFile( $path ) ) {
                    $message = 'Trying to load file that already loaded. Class name: '. $className;
                    $exception = new \LoadException( $message, 1 );
                    $exception->setClassName( $className )
                        ->setSearchPaths( $paths );
                    throw $exception;
                }

                require $path;

                self::_registerLoadedFile( $path );
                self::_clearClassSpecifiedPaths();

                return true;
            }
        }

        return false;
    }

}