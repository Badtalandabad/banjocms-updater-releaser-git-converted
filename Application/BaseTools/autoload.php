<?php

/**
 * Autoload function
 *
 * @param   string  $className  Full (with the namespaces) name of the class
 *
 * @throws  LoadException   If file can't be found
 *
 * @return  string
 */
function autoload( $className )
{
    if ( ! \Loader::load( $className ) ) {
        $message = 'Class file was not found. Class name: '
            . $className;

        $exception = new \LoadException( $message, 1 );

        $exception->setSearchPaths( \Loader::getSearchPaths( $className ) );

        throw $exception;
    }
}

spl_autoload_register( 'autoload' );
