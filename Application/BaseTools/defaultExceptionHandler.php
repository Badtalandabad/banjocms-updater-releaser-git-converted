<?php
/**
 * Default exception handler
 *
 * @param   \Exception\ExceptionAbstract    $exception
 */
$defaultExceptionHandler = function( $exception ) {
    $message = $exception->getMessage();

    $response = new JsonResponse();
    $response->setError();
    $response->setErrorMessage( $message );

    echo $response;
};

set_exception_handler( $defaultExceptionHandler );