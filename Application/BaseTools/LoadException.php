<?php

require EXCEPTION_ABSTRACT_PATH;

/**
 * Class LoadException
 * Describes exception, which comes on loading some class
 */
class LoadException extends \Exception\ExceptionAbstract
{
    /**
     * Paths, where loader tried to find class file
     *
     * @var array
     */
    private $_searchPaths = array();

    /**
     * Name of the class, which loader did not find
     *
     * @var string
     */
    private $className = '';

    /**
     * Sets search paths
     *
     * @param   array   $paths  Paths
     *
     * @return  LoadException
     */
    public function setSearchPaths( $paths )
    {
        $this->_searchPaths = $paths;

        return $this;
    }

    /**
     * Returns search paths
     *
     * @return  array
     */
    public function getSearchPaths()
    {
        return $this->_searchPaths;
    }

    /**
     * Sets class name(which was tried to load)
     *
     * @param   string  $className  Name of the class, which loader did not find
     *
     * @return  LoadException
     */
    public function setClassName( $className )
    {
        $this->className = $className;

        return $this;
    }

    /**
     * Returns name of the class, which loader did not find
     *
     * @return  string
     */
    public function getClassName()
    {
        return $this->className;
    }

}