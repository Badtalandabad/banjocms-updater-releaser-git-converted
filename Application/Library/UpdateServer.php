<?php

/**
 * Class UpdateServer
 * Server for system updating
 */
class UpdateServer
{
    /**
     * Returns file hashes in json format
     *
     * @param   string  $version    Old release version (e.g. 1.34.2)
     *
     * @throws  \Exception\UpdateServerException    If invalid version was given
     *
     * @return  string
     */
    public function getReleaseHashesTask( $version )
    {
        if ( ! \SReleaser::isVersion( $version ) ) {
            $exceptionMessage = 'Invalid version in getReleaseHashes task.';
            throw new \Exception\UpdateServerException( $exceptionMessage );
        }

        $hashesFilePath = \SReleaser::getHashesFilePath( $version );

        if ( file_exists( $hashesFilePath ) ) {
            $hashesJson = file_get_contents( $hashesFilePath );
            //TODO: Added method "setEncodedJson" to JsonResponse class instead of doing this
            $hashes = json_decode( $hashesJson, true );
        } else {
            $exceptionMessage = 'Hashes file for version "' . $version . '"'
                . ' does not exist.';
            throw new \Exception\UpdateServerException( $exceptionMessage );
        }

        if ( ! array_key_exists( \SReleaser::HASHES_FILE_FILES_KEY, $hashes ) ) {
            $exceptionMessage = 'Hashes file does not contain "'
                . \SReleaser::HASHES_FILE_FILES_KEY . '" section.';
            throw new \Exception\UpdateServerException( $exceptionMessage );
        }

        return $hashes[\SReleaser::HASHES_FILE_FILES_KEY];
    }

    /**
     * Returns file hashes in json format
     *
     * @param   string  $version    Old release version (e.g. 1.34.2)
     *
     * @throws  \Exception\UpdateServerException    If invalid version was given
     *
     * @return  string
     */
    public static function getReleasePackHashTask( $version )
    {
        if ( ! \SReleaser::isVersion( $version ) ) {
            $exceptionMessage = 'Invalid version in getReleaseHashes task.';
            throw new \Exception\UpdateServerException( $exceptionMessage );
        }

        $releaseFilePath = \SReleaser::getReleaseFilePath( $version );

        if ( ! file_exists( $releaseFilePath ) ) {
            $exceptionMessage = 'Release file for version "' . $version . '"'
                . ' does not exist.';
            throw new \Exception\UpdateServerException( $exceptionMessage );
        }

        $releaseFileHash = md5( file_get_contents( $releaseFilePath ) );

        return $releaseFileHash;
    }

    /**
     * Checks if update is needed.
     *
     * @param   string  $remoteSystemVersion    Release version of remote system (e.g. 1.34.2)
     *
     * @return  bool
     */
    public function checkForUpdateTask( $remoteSystemVersion )
    {
        $releaseVersion = \SReleaser::getReleaseVersion();

        $compare = strcasecmp( $releaseVersion, $remoteSystemVersion );

        if ( $compare > 0 ) {
            return $releaseVersion;
        }

        return false;
    }

    /**
     * Runs updating process. Opens release pack and sends its content.
     *
     * @throws  \Exception\UpdateServerException    If cannot open release pack.
     *
     * @return  void
     */
    public function updateTask()
    {
        $releaseVersion = \SReleaser::getReleaseVersion();

        $filePath = \SReleaser::getReleaseFilePath();
        $releaseFile = file_get_contents( $filePath );
        if ( ! $releaseFile ) {
            //TODO: Next lines are unused
            $responseMessage = 'Update error. Failed in finding an update for version: '
                . $releaseVersion;
            $response = new \JsonResponse();
            $response->setError()
                ->setErrorMessage( $responseMessage );

            $exceptionMessage = 'Cannot get release package. Version: '
                . \SReleaser::getReleaseVersion() . '. File path: ' . $filePath;
            throw new \Exception\UpdateServerException( $exceptionMessage );
        }

        echo $releaseFile;
    }

    public function sendResponse( $response )
    {
        //TODO: if error, send http error header

        echo $response;
    }

}