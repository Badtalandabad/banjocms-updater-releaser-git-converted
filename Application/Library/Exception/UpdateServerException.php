<?php

namespace Exception;

/**
 * Class UpdateServerException
 * Exception in Update Server
 */
class UpdateServerException extends FatalException
{
    /**
     * Initialisation
     *
     * @return  void
     */
    public function init()
    {
        $this->message = 'Update Server error: ' . $this->message;
    }
} 