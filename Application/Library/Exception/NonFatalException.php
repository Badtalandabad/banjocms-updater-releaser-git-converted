<?php

namespace Exception;


/**
 * Class NonFatalException
 * Children of the class should be caught
 *
 * @package Exception
 */
class NonFatalException extends ExceptionAbstract
{
    /**
     * Channels of the exception reporting. See possible values in the base class
     *
     * @var array
     */
    protected $_reportLevel = array();

}