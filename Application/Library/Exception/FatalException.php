<?php

namespace Exception;


/**
 * Class FatalException
 * Children should not be caught
 *
 * @package Exception
 */
class FatalException extends ExceptionAbstract
{
    /**
     * Channels of the exception reporting. See possible values in the base class
     *
     * @var array
     */
    protected $_reportLevel = array('log');

} 