<?php

namespace Exception;

/**
 * Class ExceptionAbstract
 * Exception abstract declaration.
 * It is not recommend to extend from this class straightly,
 * use FatalException or NonFatalException
 *
 * @package Exception
 */
abstract class ExceptionAbstract extends \Exception
{
    /**
     * Doing default report switcher
     *
     * @var bool
     */
    protected $_doDefaultReport = true;

    /**
     * Write to log or not
     *
     * @var bool
     */
    protected $_writeLogReport = false;

    /**
     * Show exception message to user or not
     *
     * @var bool
     */
    protected $_showToUser = false;

    /**
     * Use html markup in exception message or not
     *
     * @var bool
     */
    protected $_useHtmlInMessage = false;

    /**
     * Constructor. Final method, for initialisation use init() method.
     *
     * @param   string  $message        Exception message
     * @param   bool    $writeLogReport Write message to log or not.
     * @param   bool    $showToUser     Show warning message to the user. User will see it in the next
     *                                  display task execution whether current program flow was stopped or not.
     */
    final public function __construct( $message, $writeLogReport = false, $showToUser = false )
    {
        $this->message = $message;

        $this->init();

        if ( $this->_doDefaultReport ) {
            $this->_report();
        }
    }

    /**
     * Reports about exception by chosen methods
     *
     * @return  array
     */
    protected function _report()
    {
        if ( $this->_showToUser ) {
            \ErrorReporter::addWarning( $this->message );
        }
        if ( $this->_writeLogReport ) {
            \SLogger::writeLog( $this->message );
        }
    }

    /**
     * Initialisation
     */
    public function init()
    {

    }

    /**
     * Switch handling html in exception message.
     * Works only for SystemException user report.
     *
     * @param   bool    $state  On or off
     *
     * @return  $this
     */
    public function turnHtml( $state )
    {
        $this->_useHtmlInMessage = (bool)$state;

        return $this;
    }

    /**
     * Returns html in message using flag.
     * Handling html in exception message works only
     * for SystemException user report.
     *
     * @return  bool
     */
    public function getHtmlState()
    {
        return $this->_useHtmlInMessage;
    }
}