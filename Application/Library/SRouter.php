<?php

/**
 * Class SRouter
 * Contains methods for routing between
 * different application parts
 */
class SRouter
{
    /**
     * Returns link to the certain task
     *
     * @param   array   $params Get parameters for the describing what to do
     *
     * @return  string
     */
    public static function route( $params = array() )
    {
        $uri = explode( '/', $_SERVER['REQUEST_URI'] );
        array_pop( $uri );
        $uri = implode( '/', $uri ) . '/index.php';

        $path = 'http://' . $_SERVER['HTTP_HOST'] . $uri;

        if ( empty( $params['task'] ) ) {
            $task = DEFAULT_TASK;
        } else {
            $task = $params['task'];
        }

        $path .= '?task=' . urldecode( $task );
        foreach ( $params as $name => $value ) {
            if ( $name == 'task' ) {
                continue;
            }
            $path .= '&' . urldecode( $name ) . '=' . urldecode( $value );
        }

        return $path;
    }

    /**
     * Redirect to the url with certain task
     *
     * @param   array   $params Parameters for the task
     *
     * @return  void
     */
    public static function redirect( $params = array() )
    {
        $path = self::route( $params );
        header( 'Location: ' . $path );

        exit(0);
    }

}