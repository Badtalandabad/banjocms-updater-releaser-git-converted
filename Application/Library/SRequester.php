<?php

/**
 * Class SRequester
 * Contains request management methods
 */
class SRequester
{
    /**
     * Returns request parameter
     *
     * @param   string  $fieldName      Name of the request parameter
     * @param   string  $defaultValue   Value in case the field wasn't set
     *
     * @return  string|null
     */
    public static function get( $fieldName, $defaultValue = null )
    {
        if ( empty( $_REQUEST[$fieldName] ) ) {
            $request = $defaultValue;
        } else {
            $request = $_REQUEST[$fieldName];
        }

        return $request;
    }

    /**
     * Takes request value and returns it by integer
     *
     * @param   string  $fieldName      Name of the request parameter
     * @param   string  $defaultValue   Value in case the field wasn't set
     *
     * @return  int|null
     */
    public static function getInt( $fieldName, $defaultValue = null )
    {
        return (int)self::get( $fieldName, $defaultValue );
    }

    /**
     * Sets element in the request array
     *
     * @param   string  $name   Name of the element
     * @param   string  $value  Value of the element
     *
     * @return  void
     */
    public static function setVariable( $name, $value )
    {
        $_REQUEST[$name] = $value;
    }

    /**
     * Sets session variable
     *
     * @param   string  $name   Variable name in $_SESSION array
     * @param   string  $value  Variable value
     */
    public static function setSessionVariable( $name, $value )
    {
        if ( session_status() == PHP_SESSION_NONE ) {
            session_start();
        }
        $_SESSION[$name] = $value;
    }

    /**
     * Returns session variable
     *
     * @param   string  $name   Variable name in $_SESSION array
     *
     * @return  mixed
     */
    public static function getSessionVariable( $name )
    {
        if ( session_status() == PHP_SESSION_NONE ) {
            session_start();
        }
        if ( array_key_exists( $name, $_SESSION ) ) {
            return $_SESSION[$name];
        }

        return null;
    }

    /**
     * Deletes variable from $_SESSION array
     *
     * @param   string  $name   Variable name in $_SESSION array
     *
     * @return  void
     */
    public static function deleteSessionVariable( $name )
    {
        unset( $_SESSION[$name] );
    }

}