<?php


/**
 * Class JsonResponse
 * Class is responsible for generating JSON messages.
 * It is used in cms and update server connection e.g.
 *
 * Fields:
 *  - data
 *  - isError
 *  - errorMessage
 *
 * @package Library
 */
class JsonResponse
{
    /**
     * Response data, i.e. what was requested.
     *
     * @var mixed
     */
    private $_data;

    /**
     * Error flag. If some error was occurred.
     *
     * @var bool
     */
    private $_isError = false;

    /**
     * Error message. Exist in response in case isError field is set true.
     *
     * @var string
     */
    private $_errorMessage = '';

    /**
     * Returns field value from response array by its name.
     *
     * @param   array   $arrayResponse  Response by array
     * @param   string  $fieldName      Name of the field
     * @param   bool    $useDefault     To use or not to use default value in case field does not exist.
     *                                  Set in false means throwing exception.
     * @param   mixed   $default        Default value.
     *
     * @throws  \Exception\NonFatalException    If field does not exist and $useDefault parameter is false
     *
     * @return  mixed
     */
    private function _getFieldFromResponseArray( $arrayResponse, $fieldName, $useDefault = false, $default = null )
    {
        if ( array_key_exists( $fieldName, $arrayResponse ) ) {
            return $arrayResponse[$fieldName];
        } elseif ( $useDefault ) {
            return $default;
        } else {
            $exceptionMessage = 'Mandatory field: "' . $fieldName .'" is missed in response';
            throw new \Exception\NonFatalException( $exceptionMessage );
        }
    }

    /**
     * Constructor
     *
     * @param   string|null $json       Json response string
     * @param   bool        $strictMode Sets strict response format,
     *                                  if some fields are missed exception would be thrown
     */
    public function __construct( $json = null, $strictMode = true )
    {
        if ( $json ) {
            $this->setResponse( $json, $strictMode );
        }
    }

    /**
     * Returns response by array.
     *
     * @return  array
     */
    public function toArray()
    {
        $response = array(
            'data'    => $this->_data,
            'isError' => $this->_isError,
        );

        if ( $this->_isError ) {
            $response['errorMessage'] = $this->_errorMessage;
        }

        return $response;
    }

    /**
     * Sets isError field true. It means error was occurred.
     *
     * @return  $this
     */
    public function setError()
    {
        //TODO: add parameter for being able to switch error state at any time
        $this->_isError = true;

        return $this;
    }

    /**
     * Returns error flag.
     *
     * @return  bool
     */
    public function isError()
    {
        return $this->_isError;
    }

    /**
     * Sets error message.
     *
     * @param   string  $errorMessage
     *
     * @return  void
     */
    public function setErrorMessage( $errorMessage )
    {
        $this->_errorMessage = $errorMessage;
    }

    /**
     * Returns error message.
     *
     * @return  string
     */
    public function getErrorMessage()
    {
        return $this->_errorMessage;
    }

    /**
     * Sets response data. What was requested.
     *
     * @param   mixed   $data
     *
     * @return  void
     */
    public function setData( $data )
    {
        $this->_data = $data;
    }

    /**
     * Returns response data. What was requested.
     *
     * @return  mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * Sets response which was given by string.
     *
     * @param   string  $strResponse    Response by string
     * @param   bool    $strictMode     Sets strict response format,
     *                                  if some fields are missed exception would be thrown
     *
     * @throws  \Exception\NonFatalException    If some fields in response are missed or redundant
     *
     * @return  void
     */
    public function setResponse( $strResponse, $strictMode = true )
    {
        //TODO: check json errors

        $useDefaults = !$strictMode;

        $arrayResponse = json_decode( $strResponse, true );

        foreach ( $arrayResponse as $fieldName => $responseField ) {
            if ( ! property_exists( $this, '_' . $fieldName ) ) {
                $exceptionMessage = 'Redundant field: "' . $fieldName .'" in response';
                throw new \Exception\NonFatalException( $exceptionMessage );
            }
        }

        $this->_isError = (bool)$this->_getFieldFromResponseArray(
            $arrayResponse, 'isError', $useDefaults
        );

        if ( $this->_isError ) {
            $this->_errorMessage = $this->_getFieldFromResponseArray(
                $arrayResponse, 'errorMessage', $useDefaults
            );
        }

        $this->_data = $this->_getFieldFromResponseArray(
            $arrayResponse, 'data', $useDefaults
        );
    }

    /**
     * Returns response in JSON format.
     *
     * @return  string
     */
    public function makeJson()
    {
        $response = $this->toArray();

        return json_encode( $response );
    }

    /**
     * Returns response in JSON format.
     * It is so cause this class represents JSON messages for connection with server.
     *
     * @return  string
     */
    public function __toString()
    {
        return $this->makeJson();
    }

}