<?php


class SReleaser
{
    /**
     * Maximum recursion steps count
     * For preventing endless loop purpose
     */
    const MAX_LOOP_STEPS = 10000;

    const HASHES_FILE_DIRS_KEY = 'directories';

    const HASHES_FILE_FILES_KEY = 'files';

    /**
     * Version of the system
     *
     * @var string
     */
    private static $_releaseVersion = '0.0.5';

    /**
     * Recursion steps count
     * For preventing endless loop purpose
     *
     * @var int
     */
    private static $_loopSteps = 0;

    /**
     * Files count
     *
     * @deprecated
     *
     * @var int
     */
    private static $_filesCount = 0;

    /**
     * Elements to exclude from scanning in every directory
     *
     * @var array
     */
    private static $_excludedEntries = array(
        '.', '..',
        'configuration.ini', //TODO: make it possible to exclude file explicitly
    );

    /**
     * Elements to exclude from scanning
     * in the root directory
     *
     * @var array
     */
    private static $_rootExcludedEntries = array(
        '.', '..',
        'log', 'tmp', 'error_log',
    );

    /**
     * File hashes
     *
     * @var array
     */
    private static $_hashes = array(
        self::HASHES_FILE_DIRS_KEY => array(),
        self::HASHES_FILE_FILES_KEY => array(),
    );

    /**
     * Exclude element from scanning or not
     *
     * @param   string  $entry  Element name
     * @param   bool    $isRoot Entry is placed in root directory or not
     *
     * @return  bool
     */
    private static function _isExcludedEntry( $entry, $isRoot = false )
    {
        if ( $isRoot ) {
            $result = in_array( $entry, self::$_rootExcludedEntries );
        } else {
            $result = in_array( $entry, self::$_excludedEntries );
        }

        return $result;
    }

    /**
     * If directory was given, scans it recursive.
     * If file was given, returns its hash.
     *
     * @param   string  $releaseDir Directory containing release files
     * @param   string  $baseObject Relative path to the element (directory or file)
     *
     * @throws  \Exception\UpdateServerException    If too much steps were in loop.
     *
     * @return  array|string
     */
    private static function _scanEntryRecursive( $releaseDir, $baseObject )
    {
        if ( ++self::$_loopSteps > self::MAX_LOOP_STEPS ) {
            throw new \Exception\UpdateServerException( 'Too much loop steps in scanEntryRecursive.' );
        }
        $fullBaseObjectPath = $releaseDir . '/' . $baseObject;

        if ( is_dir( $fullBaseObjectPath ) ) {
            $baseEntries = scandir( $fullBaseObjectPath );
            self::$_hashes[self::HASHES_FILE_DIRS_KEY][] = $baseObject;
            foreach ( $baseEntries as $entry ) {
                if ( self::_isExcludedEntry( $entry ) ) {
                    continue;
                }
                $entryPath = $baseObject . '/' . $entry;
                self::_scanEntryRecursive( $releaseDir, $entryPath );
            }
        } else {
            //TODO: use for statistic
            self::$_filesCount++;
            self::$_hashes[self::HASHES_FILE_FILES_KEY][$baseObject] = md5( file_get_contents( $fullBaseObjectPath ) );
        }
    }

    /**
     * Recursive function for packing files of the system into a zip archive.
     *
     * @param   string      $releaseDir Directory containing release files
     * @param   \ZipArchive $zip        ZipArchive object. Zip pack of the files of the system.
     * @param   array       $hashes     Array with file hashes, represents file tree.
     *
     * @deprecated
     *
     * @return  void
     */
    private static function _packToZipRecursive( $releaseDir, $zip, $hashes )
    {
        foreach ( $hashes as $entryName => $entry ) {
            if ( is_array( $entry ) ) { //is directory
                $zip->addEmptyDir( $entryName );
                self::_packToZipRecursive( $releaseDir, $zip, $entry );
            } elseif ( is_string( $entry ) ) { //is file
                $zip->addFile( $releaseDir . '/' . $entryName, $entryName );
            }
        }
    }

    /**
     * Returns path to the directory with release files i.e.
     * where are files with code which are going to be a release pack
     *
     * @return  bool|string
     */
    public static function getReleaseDevelopmentDirPath()
    {
        $releaseDir = \Configuration::getParameter( 'releaseDevelopmentDirectory' );

        if ( ! is_string($releaseDir) || $releaseDir == '' ) {
            return false;
        }

        if ( $releaseDir[0] != '/' ) {
            $releaseDir = BASE_PATH . '/' . $releaseDir;
        }

        if ( substr( $releaseDir, -1 ) == '/' && strlen( $releaseDir ) > 1 ) {
            $releaseDir = substr_replace( $releaseDir, '', -1 );
        }

        return $releaseDir;
    }

    /**
     * Returns name of release file. Just for unifying file naming.
     *
     * @param   string|null $version    Release version (e.g. 1.34.2)
     *
     * @return  bool|string
     */
    public static function getReleaseFileName( $version = null )
    {
        if ( $version === null ) {
            $version = self::getReleaseVersion();
        } elseif ( ! self::isVersion( $version ) ) {
            return false;
        }

        $fileName = 'release-pack-v.' . $version . '.zip';

        return $fileName;
    }

    /**
     * Returns name of file which contains hashes of release files.
     *
     * @param   string|null $version    Release version (e.g. 1.34.2)
     *
     * @return  bool|string
     */
    public static function getHashesFileName( $version = null )
    {
        if ( $version === null ) {
            $version = self::getReleaseVersion();
        } elseif ( ! self::isVersion( $version ) ) {
            return false;
        }

        $fileName = 'hashes-v.' . $version . '.hsh';

        return $fileName;
    }

    /**
     * Returns full path to release file.
     *
     * @param   string|null $version    Release version (e.g. 1.34.2)
     *
     * @return  bool|string
     */
    public static function getReleaseFilePath( $version = null )
    {
        $fileName = self::getReleaseFileName( $version );
        if ( ! $fileName ) {
            return false;
        }

        $filePath = RELEASE_DIR . '/' . $fileName;

        return $filePath;
    }

    /**
     * Returns full path to file which contains hashes of release files.
     *
     * @param   string|null $version    Release version (e.g. 1.34.2)
     *
     * @return  bool|string
     */
    public static function getHashesFilePath( $version = null )
    {
        $fileName = self::getHashesFileName( $version );
        if ( ! $fileName ) {
            return false;
        }

        $filePath = RELEASE_DIR . '/' . $fileName;

        return $filePath;
    }

    /**
     * Returns true if valid release version was given.
     *
     * @param   string  $version    Release version (e.g. 1.34.2)
     *
     * @return  bool
     */
    public static function isVersion( $version )
    {
        $isValid = false;

        if ( strlen($version) > 4
            && strlen($version) < 9
            && preg_match( '/\d\.\d\.\d/', $version )
        ) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Sets release version
     *
     * @param   string  $releaseVersion Release version
     *
     * @return  void
     */
    public static function setReleaseVersion( $releaseVersion )
    {
        self::$_releaseVersion = $releaseVersion;
    }

    /**
     * Returns release version
     *
     * @return  string
     */
    public static function getReleaseVersion()
    {
        return self::$_releaseVersion;
    }

    /**
     * Scans system, and returns file structure by array.
     * Result has structure like following:
     * array(
     *     'directoryName' => array(
     *         'fileName' => 'fileHash',
     *         'fileName' => 'fileHash',
     *         'directoryName' => array(...),
     *         ...
     *     ),
     *     'fileName' => 'fileHash',
     *     ...
     * )
     *
     * @param   string  $releaseDir Directory containing release files
     *
     * @return  array
     */
    public static function calcReleaseFileHashes( $releaseDir )
    {
        $rootEntries = scandir( $releaseDir );

        foreach ( $rootEntries as $entry ) {
            if ( self::_isExcludedEntry( $entry, true ) ) {
                continue;
            }
            $fullEntryPath = $releaseDir . '/' . $entry;

            if ( is_dir( $fullEntryPath ) ) {
                self::_scanEntryRecursive( $releaseDir, $entry );
            } else {
                // md5_file() function does not do what we need here
                // when contents are identical but timestamps are different,
                // we will get different hashes, what is not what we want
                self::$_hashes[self::HASHES_FILE_FILES_KEY][$entry] = md5( file_get_contents( $fullEntryPath ) );
            }
        }

        return self::$_hashes;
    }

    /**
     * Makes file hashes array, encodes it in json format and saves it in a file
     *
     * @throws  \Exception\NonFatalException    If no development directory is set
     *
     * @return  bool
     */
    public static function makeReleaseHashesStore()
    {
        $devDir = self::getReleaseDevelopmentDirPath();

        if ( ! $devDir ) {
            $exceptionMessage = 'Cannot get release development directory';
            throw new \Exception\NonFatalException( $exceptionMessage );
        }

        self::calcReleaseFileHashes( $devDir );

        $fileName = self::getHashesFilePath();
        $jsonHashes = json_encode( self::$_hashes );

        $result = file_put_contents( $fileName, $jsonHashes, LOCK_EX );

        return $result;
    }

    /**
     * Packs files of the system to zip file, using recursive function.
     *
     * @throws  \Exception\NonFatalException        If no development directory is set
     * @throws  \Exception\UpdateServerException    If cannot open file with file hashes.
     *
     * @return  bool
     */
    public static function packReleaseToZip()
    {
        //TODO: find out why it is here
        if ( empty( self::$_hashes ) ) {
            return false;
        }

        $devDir = self::getReleaseDevelopmentDirPath();

        if ( ! $devDir ) {
            $exceptionMessage = 'Cannot get release development directory';
            throw new \Exception\NonFatalException( $exceptionMessage );
        }

        $zip = new \ZipArchive();
        $filePath = self::getReleaseFilePath();

        if ( $zip->open( $filePath, \ZipArchive::OVERWRITE ) !== TRUE ) {
            throw new \Exception\UpdateServerException( 'Unable to open <' . $filePath . '>' );
        }

        foreach ( self::$_hashes[self::HASHES_FILE_DIRS_KEY] as $dirPath ) {
            $zip->addEmptyDir( $dirPath );
        }
        //TODO: add success mark in result
        foreach ( self::$_hashes[self::HASHES_FILE_FILES_KEY] as $filePath => $fileHash ) {
            $fullFilePath = $devDir . '/' . $filePath;
            if ( ! file_exists( $fullFilePath ) ) {
                $exceptionMessage = 'File <' . $fullFilePath . '> does not exist!';
                throw new \Exception\UpdateServerException( $exceptionMessage );
            } elseif ( md5( file_get_contents( $devDir . '/' . $filePath ) ) != $fileHash ) {
                $exceptionMessage = 'File <' . $fullFilePath . '> hash'
                    . ' does not match with hash in the store';
                throw new \Exception\UpdateServerException( $exceptionMessage );
            } else {
                $zip->addFile( $devDir . '/' . $filePath, $filePath );
            }
        }

        $zip->close();

        return true;
    }

}