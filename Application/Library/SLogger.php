<?php


/**
 * Class SLogger
 *
 * Logger service
 */
class SLogger
{
    private static $_logFile = 'application-error.log';

    public static function writeLog( $message )
    {
        $message = date( 'H:i:s ' ) . $message . "\n" ;

        $logFilePath = BASE_PATH . '/log/' . self::$_logFile;

        $file = fopen( $logFilePath, 'a+t' );
        fwrite( $file, $message );
        fclose( $file );
    }

}