<?php

require 'entry.php';

$command = \SRequester::get( 'command', 'showtime!' );

/** @var \UpdateServer $server */
try {
    switch ($command) {
        case 'make-release':
            echo '<strong>Version: ' . \SReleaser::getReleaseVersion() . '</strong><br />';
            if ( SReleaser::makeReleaseHashesStore() ) {
                echo 'Hashes store is done.';
            } else {
                echo 'Hashes store is not done.';
            }
            echo '<br />';
            if (SReleaser::packReleaseToZip()) {
                echo 'Release pack is done';
            } else {
                echo 'Release pack is not done';
            }
            break;

        case 'make-tmp-zip':
            $zip = new ZipArchive();
            $fileName = TEMP_DIR . '/' . $server->getReleaseFileName();
            if ($zip->open($fileName, ZipArchive::OVERWRITE) !== TRUE) {
                throw new \Exception\UpdateServerException('Unable to open <' . $fileName . '>');
            }
            $zip->addFile('path in system', 'path in zip');
            if ($zip->close()) {
                echo 'ok';
            }
            break;

        case 'showtime!':
            echo 'Hi there!';
            break;

        default:
            echo 'Unknown command: "' . htmlspecialchars($command) . '"';
            break;
    }
} catch ( \Exception $e ) {
    echo ' Fatal error.<br />' . htmlspecialchars($e->getMessage());
    echo '<br />File: <strong>' . htmlspecialchars( $e->getFile() ) . '</strong>';
    echo '<br />Line: <strong>' . (int)$e->getLine() . '</strong>';
    echo '<br />Traceback: ';
    echo '<ul><li>';
    echo str_replace( '<br />', '</li><li>', nl2br($e->getTraceAsString()) );
    echo '</li></ul>';
}

$output = ob_get_clean();

if ( $output ) {
    $style = '<style>
                body {
                    background: #434343;
                    color: #ededed;
                    text-shadow: 1px 1px 2px #000;
                }
                ul {
                    margin: 0;
                }
                strong {
                    text-shadow: 1px 1px 3px #000;
                }
                a {
                    color: #e8bf6a;
                }
            </style>';
    $output = $style . $output;
    echo $output;
}
